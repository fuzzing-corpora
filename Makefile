
all:
	@echo "Nothing to do"

dist:
	git archive \
		--format=tar \
		--prefix=tor-fuzzing-corpora/ \
		HEAD . \
	| xz -9 -c \
	> "tor-fuzzing-corpora-`git rev-parse HEAD`-`date -u +%Y%m%d`.tar.xz"
